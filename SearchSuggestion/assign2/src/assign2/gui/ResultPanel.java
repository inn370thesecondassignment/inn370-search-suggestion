package assign2.gui;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Create a TextArea for the ResultPanel
 * 
 * @author Mingzhuo LIU N8735751
 *
 */
@SuppressWarnings("serial")
public class ResultPanel extends JPanel {
	
	private JTextArea textDisplay;
	private JScrollPane textDisplayScroll;
	public ResultPanel() {
		//Default textDisplay
		textDisplay = new JTextArea(30,70);
		textDisplay.setEditable(false);
		textDisplay.setLineWrap(true);
		textDisplay.setBackground(Color.BLACK);
		textDisplay.setForeground(Color.GREEN);

		textDisplayScroll = new JScrollPane(textDisplay);
		add(textDisplayScroll);
		textDisplay.setText("Welcome to Search Suggestion!\n\n" +
							"Please entre your queries in the top textarea and press 'Search' button!\n\n" +
							"================================================================================\n" +
							"Valid inputs allowed:\n" +
							"	Upper and lower case alphabetic characters" + "\n" + 
							"	Numerics: 0 1 2 3 4 5 6 7 8 9" + "\n" + 
							"	Single quotes: �� BUT not �� or `" + "\n" + 
							"	Commas: , as the phrase separator only.\n" + 
							"Examples of valid inputs include:\n" +
							"	My hovercraft is, to be or, woof, I��m singin�� in, sound of, all that\n" +
							"Invalid inputs include:\n" +
							"	$%#%# that ^%^$#, alpha$beta, gamma & woof\n" + 
							"================================================================================\n\n\n");
	}
	public void appendText(String text){
		
		//achieve appendDislay method
		textDisplay.setText(textDisplay.getText() + text);
		
	}
}
