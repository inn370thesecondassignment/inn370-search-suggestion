package assign2.gui;

import java.awt.*;

import assign2.ngram.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

/**
 * This class is used to construct the frame of GUI
 * @author Mingzhuo LIU n8735751
 *
 */
@SuppressWarnings("serial")
public class NGramGUI extends JFrame implements ActionListener, Runnable{
	
	//Default size of window
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	
	//panels for the Frame
	private JPanel btmPanel;
	private JPanel textualInfo;
	private JPanel inputPanel;
	private JPanel chartPanel;
    private JPanel displayPanel;
    private JPanel ChartsInfo;
	
	//Input text
	private JTextField textInput;
	
	//Buttons
	private JButton searchButton;
	private JButton chartButton;
	
	//The input content from
	private String InputContent;
								
	//This boolean is used to control if the charts need to create
	boolean chartsCreated = false;
	
	//Create a int variable to record the number of errors
	int errors = 0;
	
	/**
	 * 
	 * @param arg0 Title of the Frame
	 * @throws HeadlessException
	 */
	public NGramGUI(String arg0) throws HeadlessException{
		super(arg0);
	}
	
	/**
	 * Construct the GUI
	 */
	private void createGUI(){
		
		//Create frame
		setSize(WIDTH,HEIGHT);
		setLocation(200,0);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
	    
	    //Create input textfield
		textInput = new JTextField(70);
		
		//Default textInput info
		textInput.setText ("Please enter your queries");
		
		//Add ancestor listener event to textInput
		textInput.addFocusListener(new FocusAdapter()
		{
			
			//When textInput gained focus select all text info and change fore color to black
			public void focusGained(FocusEvent e) 
			{
				
				if(e.getSource() == textInput)
				textInput.selectAll();
			}
		});
		
		//Create input panel
		inputPanel = new JPanel();

	    inputPanel.add(textInput);
	    
	    //Add input panel to the frame of GUI
	    this.getContentPane().add(inputPanel, BorderLayout.NORTH);
	    
	    //Create displayPanel panel and this panel used for textualInfo and ChartPanel 
	  	displayPanel = new JPanel();
	  			
	  	//Create textualInfo and set its visible is true
	  	textualInfo = new ResultPanel();
	  	textualInfo.setVisible(true);

	  	//Add textualInfo to displayPanel panel
	  	displayPanel.add(textualInfo);
	  	
	  	//Add displayPanel panel to the frame of GUI
	  	this.getContentPane().add(displayPanel,BorderLayout.CENTER);
	  	
	  	//Create btmPane and this panel used for search and chart buttons
	  	btmPanel = new JPanel();

	  	//Create search button and named Search
	  	searchButton = new JButton("Search");
	  	searchButton.addActionListener(this);
	  	
	  	//Add searchButton to btmPanel
	  	btmPanel.add(searchButton);
	  	
	  	//Create chart button and named displayPanel Chart
	 	chartButton = new JButton("Display Charts");
	 	chartButton.setEnabled(false);
	 	chartButton.addActionListener(this);
	 	
	  	//Add chartButton to btmPanel
	 	btmPanel.add(chartButton);
	 	
	 	//Add btmPanel to the frame of GUI
	 	this.getContentPane().add(btmPanel,BorderLayout.SOUTH);
	 	
	 	
	}
	
	/**
	 * This method is to display the result to the screen
	 * @param phrases -input a phrases string array which is divide the user 
	 * 					inputs into single phrases
	 */
	private void textualDisplay(String[] phrases){
		
		//Create results string object to store the results which is from service		
		String results = "";
		
		//Initial errors
		errors = 0;
		
		//Use for loop to store phrases
		for (int i = 0; i < phrases.length; i++)
		{
			//Create a new ngramStroe
			NGramStore ngramStore = new NGramStore();
			
			try 
			{
			
				ngramStore.getNGramsFromService(phrases[i], 5);
				
				if (ngramStore.toString().trim()!="")
				{
					//add the results to results String 
					results += "NGram Results for Query: " + phrases[i] + "\n\n" + ngramStore.toString() + "\n";
				}
			}
			catch (NGramException exception) 
			{
				results += "NGram Results for Query: " + phrases[i] + "\n\n" + exception.getMessage() + "\n";
				errors += 1;
			}
			catch (Exception exception)
			{
				((ResultPanel) textualInfo).appendText("Unhandled exception - " + exception.getMessage() + "\n");
				errors += 1;
				throw new RuntimeException(exception);
			}
		}
		//Output the results
		((ResultPanel) textualInfo).appendText(results);
		((ResultPanel) textualInfo).appendText("=====================================================================\n\n");
	}	
	
	/**
	 * This method is to create charts and display them to the screen
	 * 
	 * @param phrases -input a phrases string array which is divide the user 
	 * 					inputs into single phrases
	 */
	private void chartsDisplay(String[] phrases){
		
		
		//Create the ChartsInfo panel which is used to contain charts
		ChartsInfo = new JPanel();
		
		//Default the ChartsInfo panel
		ChartsInfo.setLayout(new GridLayout(2,3));
		//Use for loop to store phrases
		for (int i = 0; i < phrases.length; i++)
		{
			//Create a new ngramStroe
			NGramStore ngramStore = new NGramStore();
			
			try 
			{
				
				ngramStore.getNGramsFromService(phrases[i], 5);
				
				//If get information from service 
				if (ngramStore.toString().trim()!="")
				{	
					NGramContainer ngramContainer = ngramStore.getNGram(phrases[i]);
					
					//Create the pred string array to store predictions 
					String[] pred= ngramContainer.getPredictions();
					
					//Create the prob double array to store probabilities
					Double[] prob = ngramContainer.getProbabilities();
					
					//Create chartPanel
					chartPanel = new ChartPanel("5-grams", phrases[i], pred, prob);
					
					//Add the chartPanel to the ChartsInfo panel
					ChartsInfo.add(chartPanel);				
				}
				
				
			}
			catch (NGramException exception) 
			{
				((ResultPanel) textualInfo).appendText("NGram Results for Query: " + phrases[i] + "\n\n"
														+ "Its chart can not be created!!\n" + 
														"--------------------------------------------------------------------------------------\n\n");
			}
			catch (Exception exception)
			{
				((ResultPanel) textualInfo).appendText("Unhandled exception - " + exception.getMessage() + "\n");
				throw new RuntimeException(exception);
			}
		}
		//Add the ChartsInfo to the displayPanel panel
		displayPanel.add(ChartsInfo);
		
		//Change chartsCreated to true
		chartsCreated = true;

		//Set textualInfo panel is not visible
		textualInfo.setVisible(false);
	}
	
	/**
	 * check if the input content is illegally
	 * @return if it is legally return false otherwise return true
	 */
	private boolean LegalChars(){
		
		//Get input from textInput
		InputContent = textInput.getText();
		
		//It is used to store the ASCII code which is input from user in the byte array
		byte[] InputASCIIDec = InputContent.getBytes();
		for (int i = 0; i<InputASCIIDec.length; i++)
		{
			//Check if the input content is legally
			if (!(((64<InputASCIIDec[i])&&(InputASCIIDec[i]<91))||((96<InputASCIIDec[i])&&(InputASCIIDec[i]<123))
			||(InputASCIIDec[i]==32)||(InputASCIIDec[i]==39)||(InputASCIIDec[i]==44)))
			{
				return true;				
			}
		}
		return false;
	}
	
	/**
	 * This method is to achieve the event of buttons
	 */
	public void actionPerformed(ActionEvent e) {
		
		//Get which Button is clicked
		String ButtonStr = e.getActionCommand();
		
		//Get input from textInput
		InputContent = textInput.getText();
		
		//When search button is been clicked
		if(ButtonStr.equals("Search")){			
			
			if (InputContent.trim().equals("Please entre your queries")|| InputContent.trim().equals("")) 
			{
				//If the InputContent is default message or empty, display error message
				((ResultPanel) textualInfo).appendText("Empty input! \nPlease enter queries again!\n\n");
				
				//Change the color to red when there has error
				textualInfo.setBackground(Color.RED);
				
				//Set chartButton is unavailable
				chartButton.setEnabled(false);
			}
			else if (LegalChars())
			{
				//If the InputContent has illegal input, display error message
				((ResultPanel) textualInfo).appendText("Illegal input! \nPlease enter queries again!\n\n");
				
				//Change the color to red when there has error
				textualInfo.setBackground(Color.RED);
				
				//Set chartButton is unavailable
				chartButton.setEnabled(false);
			}
			else
			{
				
				//Use split methods divide the input into a string array, the separatrix is comma
				String[] phrases = InputContent.split(",");
				
				//Use textualDisplay method to display results
				textualDisplay(phrases);
				
				if (errors == 0)
				{
					//Set chartButton is available
					chartButton.setEnabled(true);
					
					//Change color to green when app runs successful
					textualInfo.setBackground(Color.GREEN);
					
					//Change chartsCreated to false
					chartsCreated = false;
				}
				else if (errors < phrases.length)
				{
					//Set chartButton is available
					chartButton.setEnabled(true);
					
					//Change color to yellow when app runs partially successful
					textualInfo.setBackground(Color.YELLOW);
					
					//Change chartsCreated to false
					chartsCreated = false;
				}
				else
				{
					//Set chartButton is available
					chartButton.setEnabled(false);
					
					//Change color to red when app runs failed
					textualInfo.setBackground(Color.RED);
					
					//Change chartsCreated to false
					chartsCreated = false;
				}
			}
		}
		else if (ButtonStr.equals("Display Charts"))
		{
			//If charts not been created
			if (!chartsCreated){
				
				//Use split methods divide the input into a string array, the separatrix is comma
				String[] phrases = InputContent.split(",");
			
				//Use chartsDisplay method to create charts
				chartsDisplay(phrases);
			}
			else
			{
				//Set ChartsInfo panel is visible
				ChartsInfo.setVisible(true);
				
				//Set textualInfo panel is not visible
				textualInfo.setVisible(false);	
			}
			
			//Set the window size
        	setExtendedState(JFrame.MAXIMIZED_BOTH);
        	
        	//Change the chartButton text
        	chartButton.setText("Display Text");
        	
        	//Set searchButton is not available
        	searchButton.setEnabled(false);
		}
		else if (ButtonStr.equals("Display Text"))
		{
			//Set the window size
			setExtendedState(JFrame.NORMAL);
			
			//Set ChartsInfo panel is not visible
			ChartsInfo.setVisible(false);
			
			//Set textualInfo panel is visible
			textualInfo.setVisible(true);
			
			//Change the chartButton text
			chartButton.setText("Display Charts");
			
			//Set searchButton is available
			searchButton.setEnabled(true);
		}
	}
	
	public void run() {
		
		//Use createGUI method to create GUI
		createGUI();
		
		//Add window Listener to set the focus to search button
		this.addWindowListener(new WindowAdapter()
		{
		       public void windowOpened(WindowEvent e) 
		       {
		              searchButton.requestFocus();
		       }
		});
		this.setMaximumSize(new Dimension(WIDTH,HEIGHT));
		this.setMinimumSize(new Dimension(WIDTH,HEIGHT));
		this.pack();
		this.setVisible(true);
	}
}
