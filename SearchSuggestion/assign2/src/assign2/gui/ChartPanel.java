package assign2.gui;

import java.awt.*;
import javax.swing.*;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;



/**
 * Create a JFreeChart for the ChartPanel
 * 
 * @author Mingzhuo LIU N8735751
 *
 */
@SuppressWarnings("serial")
public class ChartPanel extends JPanel{

	
	public ChartPanel(String chartTitle, String inputContent, String[] words, Double[] prob){
		 
	     //Create dataset
		 CategoryDataset dataset = createDataset(prob, inputContent, words);
	     // create the chart
	     JFreeChart chart = createChart(dataset, chartTitle);
	     // put the chart into a panel
	     org.jfree.chart.ChartPanel chartPanel = new org.jfree.chart.ChartPanel(chart);
	     // default size
	     chartPanel.setPreferredSize(new java.awt.Dimension(450, 300));
	     // add barchartPanel to ChartPanel
	     add(chartPanel);
	}


	/*
     * Creates a dataset 
     */
    private  DefaultCategoryDataset createDataset(Double[] prob,String inputContent,String[] words){
    	
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        //use for loop to add values
        for (int i = 0; i<words.length; i++){
        	
        	dataset.setValue(prob[i], inputContent, words[i]);
        }
        return dataset;
    }
    
    
    /*
     * Creates a chart
     */
    private JFreeChart createChart(CategoryDataset dataset, String title) {
    	
    	JFreeChart chart = ChartFactory.createBarChart3D(
    		title, 
     		"Phrase _____",
    		"Probability",
    		dataset, 
    		PlotOrientation.VERTICAL, 
    		true, 
    		false, 
    		false
    	);
    	
    	chart.setBackgroundPaint(Color.LIGHT_GRAY);
    	chart.setBorderVisible(true);
    	chart.setBorderPaint(Color.BLACK);
    	chart.getTitle().setPaint(Color.BLUE); 
    	CategoryPlot p = chart.getCategoryPlot(); 
    	p.setRangeGridlinePaint(Color.red); 
    			
        return chart;
    }
	

}
