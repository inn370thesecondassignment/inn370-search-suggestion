package assign2.ngram;

import static org.junit.Assert.*;

import java.lang.reflect.Array;
import java.text.DecimalFormat;

import org.junit.Test;
import org.junit.Before;

/**
 * @author Hunaynah Al Naamany N8820848
 * 
 */


/*
 * I think we should add some consturctor tests, one is the constructor1 and another one is 
 * constructor2
 * Input all paras which are the constructor is required and then test the data
 * whether or not be stored in the container correctly.
 * 
 * Mingzhuo Liu
 */

public class NGramNodeTest {
	NGramNode contextNode, wordsNode;
	String context = "be or not to";
	String[] words = {"to", "be", "or", "not"};
	String[] predictions = {"be", "mention", "exceed", "say", "the"};
	Double[] probabilities = { 0.136059, 0.066563, 0.032759, 0.028824, 0.024524 };
	private NGramNode toTest;
	
	/*Setting up declaring two NGramNodes, for context and for words*/
	@Before 
	public void setUp() throws NGramException{
		contextNode = new NGramNode(context, predictions, probabilities);
		wordsNode = new NGramNode(words, predictions, probabilities);
		toTest = new NGramNode(words, predictions, probabilities);
		}
	
	/*
	 *Testing getProbabilities and checking if equal with the default probabilities 
	 */
	@Test
	public void getProbabilities() {
		assertArrayEquals(probabilities, contextNode.getProbabilities());
		}
	
	/*
	 *Testing getProbabilities with valid inputs 
	 */
	@Test
	public void getValidProbabilitiesInput() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] validProbabilities={0.1,0.2,0.3,0.4,0.5};
		probabilitiesNode = new NGramNode(context, predictions, validProbabilities);
		probabilitiesNode.getProbabilities();
		}
	
	/*
	 *Testing getProbabilities with empty value
	 *using empty prediction
	 */
	@Test
	(expected = NGramException.class)
	public void getEmptyProbabilities() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] emptyProbabilities={};
		String[] emptyPredictions = {};
		probabilitiesNode = new NGramNode(context, emptyPredictions, emptyProbabilities);
		probabilitiesNode.getProbabilities();
		}
	
	/*
	 *Testing getProbabilities with null value
	 *using null prediction
	 */
	@Test
	(expected = NGramException.class)
	public void getNullProbabilities() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] nullProbabilities={null};
		String[] nullPredictions = {null};
		probabilitiesNode = new NGramNode(context, nullPredictions, nullProbabilities);
		probabilitiesNode.getProbabilities();
		}
	
	/*
	 *Testing getProbabilities with values more than one
	 */
	@Test
	(expected = NGramException.class)
	public void getProbabilitiesMoreThanOne() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] highProbabilities={1.0,2.0};
		String[] highPredictions = {"be", "mention"};
		probabilitiesNode = new NGramNode(context, highPredictions, highProbabilities);
		probabilitiesNode.getProbabilities();
		}
	
	/*
	 *Testing getProbabilities with values less than zero
	 */
	@Test
	(expected = NGramException.class)
	public void getNegativeProbabilities() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] negativeProbabilities={-0.5,0.5,0.32,0.4,0.5};
		probabilitiesNode = new NGramNode(context, predictions, negativeProbabilities);
		probabilitiesNode.getProbabilities();
		}
	
	/*
	 *Testing getProbabilities with value of zero
	 */
	@Test
	(expected = NGramException.class)
	public void getZeroProbabilities() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] zeroProbabilities={0.0,0.5,0.32,0.4,0.5};
		probabilitiesNode = new NGramNode(context, predictions, zeroProbabilities);
		probabilitiesNode.getProbabilities();
		}
	
	/*
	 *Testing setProbabilities with valid input 
	 */
	@Test
	public void setValidProbabilities() throws NGramException {
		Double[] validProbabilities = {0.232, 0.3234};
		contextNode.setProbabilities(validProbabilities);
		}
	
	/*
	 * Testing valid values of probabilities 
	 */
	@Test
	public void validProbabilities() throws NGramException {
		Double[] probabilities = {0.1}, validProb;
		validProb = new Double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++)
		{
			if (probabilities[i] > 1.0 || probabilities[i] == null || probabilities[i] < 0 ||
					probabilities[i] == 0 || probabilities[i] < 0)
			{
				throw new NGramException("Invalid entry");
				}
			else
			{
				validProb[i] = probabilities[i];
				}
			}
		}
		
	/*
	 *Testing setProbabilities with empty value
	 *using empty prediction
	 */
	@Test
	(expected = NGramException.class)
	public void setEmptyProbabilities() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] emptyProbabilities={};
		String[] emptyPredictions = {};
		probabilitiesNode = new NGramNode(context, emptyPredictions, emptyProbabilities);
		probabilitiesNode.setProbabilities(emptyProbabilities);
		}
	
	/*
	 *Testing setProbabilities with null value
	 *using null prediction
	 */
	@Test
	(expected = NGramException.class)
	public void setNullProbabilities() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] nullProbabilities={null};
		String[] nullPredictions = {null};
		probabilitiesNode = new NGramNode(context, nullPredictions, nullProbabilities);
		probabilitiesNode.setProbabilities(nullProbabilities);
		}
	
	/*
	 * Testing at least one entry is null in probabilities 
	 */
	@Test
	(expected=NGramException.class)
	public void setOneNullProbabilities() throws NGramException {
		Double[] probabilities = {null}, validProb;
		validProb = new Double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++)
		{
			if (probabilities[i] == null)
			{
				throw new NGramException("probabilities contains at least one entry which is null");
				}
			else
			{
				validProb[i] = probabilities[i];
				}
			}
		}
	
	/*
	 *Testing setProbabilities with values more than one
	 */
	@Test
	(expected = NGramException.class)
	public void setProbabilitiesMoreThanOne() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] highProbabilities={1.0,2.0};
		String[] highPredictions = {"be", "mention"};
		probabilitiesNode = new NGramNode(context, highPredictions, highProbabilities);
		probabilitiesNode.setProbabilities(highProbabilities);
		}
	
	/*
	 * Testing greater than one value of probabilities 
	 */
	@Test
	(expected=NGramException.class)
	public void setGreaterThanOneProbabilities() throws NGramException {
		Double[] probabilities = {1.1}, validProb;
		validProb = new Double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++)
		{
			if (probabilities[i] > 1.0)
			{
				throw new NGramException("probabilities contains an entry which is zero");
				}
			else
			{
				validProb[i] = probabilities[i];
				}
			}
		}	
	
	/*
	 *Testing setProbabilities with values less than zero
	 */
	@Test
	(expected = NGramException.class)
	public void setNegativeProbabilities() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] negativeProbabilities={-0.5,0.5,0.32,0.4,0.5};
		probabilitiesNode = new NGramNode(context, predictions, negativeProbabilities);
		probabilitiesNode.setProbabilities(negativeProbabilities);
		}
	
	/*
	 * Testing less than zero value of probabilities 
	 */
	@Test
	(expected=NGramException.class)
	public void setLessThanZeroProbabilities() throws NGramException {
		Double[] probabilities = {-0.5}, validProb;
		validProb = new Double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++)
		{
			if (probabilities[i] < 0)
			{
				throw new NGramException("probabilities contains an entry which is zero");
				}
			else
			{
				validProb[i] = probabilities[i];
				}
			}
		}
	
	/*
	 *Testing setProbabilities with value of zero
	 */
	@Test
	(expected = NGramException.class)
	public void setZeroProbabilities() throws NGramException {
		NGramNode probabilitiesNode;
		Double[] zeroProbabilities={0.0,0.5,0.32,0.4,0.5};
		probabilitiesNode = new NGramNode(context, predictions, zeroProbabilities);
		probabilitiesNode.setProbabilities(zeroProbabilities);
		}
	/*
	 * Testing at zero value of probabilities 
	 */
	@Test
	(expected=NGramException.class)
	public void setProbabilitiesZero() throws NGramException {
		Double[] probabilities = {0.0}, validProb;
		validProb = new Double[probabilities.length];
		for (int i = 0; i < probabilities.length; i++)
		{
			if (probabilities[i] == 0)
			{
				throw new NGramException("probabilities contains an entry which is zero");
				}
			else
			{
				validProb[i] = probabilities[i];
				}
			}
		}
	
	/*
	 * Testing two constructors
	 * check Probabilities And Prediction Length
	 * then set context and words
	 * Test the data stored in the container correctly or not.
	 */
	@Test
	public void NGramNodeProbabilityAndPredictionLength() throws NGramException {
		if (predictions.length == probabilities.length)
		{
		contextNode.setContext(context);
		wordsNode.setContext(words);
		}
	}
	
	/*
	 * Testing two constructors
	 * check Probabilities And Prediction Length
	 * then set context and words
	 */
	@Test
	(expected = NGramException.class)
	public void NGramNodeInvalidProbabilityAndPredictionLength() throws NGramException {
		String[] invalidPredictions = {"be", "mention"};
		Double[] invalidProbabilities = {0.136059};
		if (invalidPredictions.length == invalidProbabilities.length)
		{
		contextNode.setContext(context);
		wordsNode.setContext(words);
		}
		else
		{
			throw new NGramException("The predictions array's length is different from probabilities array's length!!\n\n"); 
		}
	}
	
	/*
	 * Testing get Context text, by checking if getContect is equal to context string
	 */
	@Test
	public void getContext() {
		assertEquals(context, contextNode.getContext());	
		}
	
	/*
	 * Testing Context text
	 * check if getContext will get "Testing"
	 */
	@Test 
	public void getValidContext() throws NGramException {
		NGramNode contextsNode;
		String validcontext="Testing";
		contextsNode = new NGramNode(validcontext, predictions, probabilities);
		contextsNode.getContext();	
		}
	
	/*
	 * Testing Context text
	 * check if getContext will get empty string
	 */
	@Test 
	(expected = NGramException.class)
	public void getEmptyContext() throws NGramException {
		NGramNode contextsNode;
		String emptycontext="";
		contextsNode = new NGramNode(emptycontext, predictions, probabilities);
		contextsNode.getContext();	
		}
	
	/*
	 * Testing Context text
	 * check if context is empty and throw exception
	 */
	@Test 
	(expected = NGramException.class)
	public void getEmptyContextCheck() throws NGramException {
		NGramNode contextsNode;
		String emptycontext="";
		contextsNode = new NGramNode(emptycontext, predictions, probabilities);
		
		if (emptycontext.isEmpty())
		{
			throw new NGramException("The context array is empty!\n\n");
		}
		else
		{
			contextsNode.getContext();
			}
	}
	
	/*
	 * Testing Context text
	 * check getContext to get null value, it will throw exception
	 */
	@Test 
	(expected = NGramException.class)
	public void getNullContext() throws NGramException {
		NGramNode contextsNode;
		String nullcontext=null;
		contextsNode = new NGramNode(nullcontext, predictions, probabilities);
		contextsNode.getContext();	
		}
	
	/*
	 * Testing set Context
	 * set string to "Testing"
	 */
	@Test
	public void setContext() throws NGramException {
		contextNode.setContext("Testing");
		}
	
	/*
	 * Testing set Context to null, by declaring Context and assigning null as value
	 * set the context in the contextNode and check if get context is equal to null
	 */
	@Test
	(expected = NGramException.class)
	public void setNullContext() throws NGramException {
		String Context = null;
		contextNode.setContext(Context);
		assertEquals(null, contextNode.getContext());
		}
	
	/*
	 * Testing set Context to empty, by declaring Context and assigning empty value
	 * set the context in the contextNode and check if get context is equal to empty
	 */
	@Test
	(expected = NGramException.class)
	public void setEmptyContext() throws NGramException {
		String Context = "";
		contextNode.setContext(Context);
		assertEquals("", contextNode.getContext());
		}
	
	/*
	 * Testing array Context, by declaring arrayString
	 * setting in contextNode, check if getContext is equal to the stored assaryString
	 */
	@Test
	public void setArrayContext() throws NGramException {
		String[] arrayString = {"Testing", "string", "array"};
		contextNode.setContext(arrayString);
		assertEquals("Testing string array", contextNode.getContext());
		}
	
	/*
	 * Testing empty array Context, by declaring arrayString
	 * setting empty in contextNode , check if getContext is equal to the empty assaryString
	 */
	@Test
	(expected = NGramException.class)
	public void setEmptyArrayContext() throws NGramException {
		String[] arrayString = {};
		contextNode.setContext(arrayString);
		assertEquals(" ", contextNode.getContext());
		}
	
	/*
	 * Testing at least one empty array Context, by declaring arrayString
	 * setting one input as empty empty in contextNode
	 * check if getContext is equal with arraryString having at least one empty string
	 */
	@Test
	(expected = NGramException.class)
	public void setOneEmptyArrayContext() throws NGramException {
		String[] arrayString = {"","Test"};
		contextNode.setContext(arrayString);
		assertEquals(" ", contextNode.getContext());
		}
	
	/*
	 * Testing at least one null array Context, by declaring arrayString and assigning at least one 
	 * null value
	 */
	@Test
	(expected = NGramException.class)
	public void setOneNullArrayContext() throws NGramException {
		String[] arrayString = {null,"Test"};
		contextNode.setContext(arrayString);
		assertEquals(null, contextNode.getContext());
		}
	
	/*
	 * Testing null array Context, by declaring arrayString and assigning null value
	 */
	@Test
	(expected = NGramException.class)
	public void setNullArrayContext() throws NGramException {
		String[] arrayString = {null,"Test"};
		contextNode.setContext(arrayString);
		assertEquals(null, contextNode.getContext());
		}
	
	/*
	 * Testing Predictions, by retrieving Predictions
	 * check getPredictions
	 */
	@Test 
	public void getPredictions() {
		assertArrayEquals(predictions, contextNode.getPredictions());
		}
	
	/*
	 *Testing getPredictions with valid inputs 
	 */
	@Test
	public void getValidPredictionsInput() throws NGramException {
		NGramNode predictionsNode;
		String[] validPredictions={"one","two","three","four","five"};
		predictionsNode = new NGramNode(context, validPredictions, probabilities);
		predictionsNode.getPredictions();
		}
	
	/*
	 *Testing getPredictions with empty value
	 *using empty probabilities
	 */
	@Test
	(expected = NGramException.class)
	public void getEmptyPredictions() throws NGramException {
		NGramNode predictionsNode;
		Double[] emptyProbabilities={};
		String[] emptyPredictions = {};
		predictionsNode = new NGramNode(context, emptyPredictions, emptyProbabilities);
		predictionsNode.getPredictions();
		}
	
	/*
	 *Testing getPrediction with null value
	 *using null probabilities
	 */
	@Test
	(expected = NGramException.class)
	public void getNullPrediction() throws NGramException {
		NGramNode predictionsNode;
		Double[] nullProbabilities={null};
		String[] nullPredictions = {null};
		predictionsNode = new NGramNode(context, nullPredictions, nullProbabilities);
		predictionsNode.getPredictions();
		}
	
	/*
	 * Testing Predictions, by setting string values to Predictions
	 * check getPredictions with assigned values
	 */
	@Test
	public void setValidPredictions() throws NGramException {
		String[] validPredictions = {"test", "predictions"};
		contextNode.setPredictions(validPredictions);
		}
	
	/*
	 * Testing valid values of Predictions 
	 */
	@Test
	public void validPredictions() throws NGramException {
		String[] predictions = {"Testing"}, validPred;
		validPred =  new String[predictions.length];
		for (int i = 0; i < predictions.length; i++)
		{
			if (predictions[i] == null || predictions[i].isEmpty())
			{
				throw new NGramException("Invalid entry");
				}
			else
			{
				validPred[i] = predictions[i];
				}
			}
		}
	
	/*
	 * Testing Predictions, by setting Predictions to null
	 */
	@Test 
	(expected = NGramException.class)
	public void setNullPredictions() throws NGramException {
		String[] nullPredictions = null;
		contextNode.setPredictions(nullPredictions);
		}
	
	/*
	 * Testing at least one entry is null in prediction 
	 */
	@Test
	(expected=NGramException.class)
	public void setOneNullPredictions() throws NGramException {
		String[] predictions = {null}, validPred;
		validPred = new String[predictions.length];
		for (int i = 0; i < predictions.length; i++)
		{
			if (predictions[i] == null)
			{
				throw new NGramException("The predictions array contains at least one null strings!\n\n");
				}
			else
			{
				validPred[i] = predictions[i];
				}
			}
		}
	
	/*
	 * Testing Predictions, by setting Predictions to empty
	 */
	@Test 
	(expected = NGramException.class)
	public void setEmptyPredictions() throws NGramException {
		String[] emptyPredictions = {};
		contextNode.setPredictions(emptyPredictions);
		}
	
	/*
	 * Testing Predictions, by setting at least one Predictions to empty
	 */
	@Test 
	(expected = NGramException.class)
	public void setOneEmptyPredictions() throws NGramException {
		String[] predictions = {"","Test"}, validPred;
		validPred = new String[predictions.length];
		for (int i = 0; i < predictions.length; i++)
		{
			if (predictions[i].isEmpty())
			{
			throw new NGramException("The predictions array contains at least one empty strings!\n\n");
		}
		else
		{
			//Add valid data to valiPred array
			validPred[i] = predictions[i];
		}
	}
	}
	
     /*
	 * Testing empty predictions, by declaring empty prediction array
	 * setting the prediction in wordsNode , check if getprediction is empty
	 * Prediction should not be empty, exception 
	 */
	@Test
	(expected=NGramException.class)
	public void setWordsEmptyArrayPredictions() throws NGramException {
		String[] PredictionArray = {};
		wordsNode.setPredictions(PredictionArray);
		assertEquals(" ", wordsNode.getPredictions());
		}
	
	/*
	 * Testing null probabilities, by declaring probabilities with null value
	 * setting the probabilities in wordsNode , check if getProbabilities is empty
	 * Probabilities should not be null, exception 
	 */
	@Test
	(expected=NGramException.class)
	public void setWordsNullProbabilities() throws NGramException {
		Double[] Probabilities = null;
		wordsNode.setProbabilities(Probabilities);
		}

		/*
	   	 * Confirm that the API spec has not been violated through the
	   	 * addition of public fields, constructors or methods that were
	   	 * not requested
	   	 */
	   	@Test
	   	public void NoExtraPublicMethods() {
	   		//Extends Object, implements NGramContainer
	   		final int toStringCount = 1;
	   		final int NumObjectClassMethods = Array.getLength(Object.class.getMethods());
	   		final int NumInterfaceMethods = Array.getLength(NGramContainer.class.getMethods());
	   		final int NumNGramNodeClassMethods = Array.getLength(NGramNode.class.getMethods());
	   		assertTrue("obj:"+NumObjectClassMethods+":inter:"+NumInterfaceMethods+" - 1 (toString()) = class:"+NumNGramNodeClassMethods,
	   				(NumObjectClassMethods+NumInterfaceMethods-toStringCount)==NumNGramNodeClassMethods);
	   	}
	   	
	   	@Test 
	   	public void NoExtraPublicFields() {
	   	//Extends Object, implements NGramContainer
	   		final int NumObjectClassFields = Array.getLength(Object.class.getFields());
	   		final int NumInterfaceFields = Array.getLength(NGramContainer.class.getFields());
	   		final int NumNGramNodeClassFields = Array.getLength(NGramNode.class.getFields());
	   		assertTrue("obj + interface = class",(NumObjectClassFields+NumInterfaceFields)==NumNGramNodeClassFields);
	   	}
	   	
	   	@Test 
	   	public void NoExtraPublicConstructors() {
	   	//Extends Object, implements NGramContainer
	   		final int ExtraConsCount =1;
	   		final int NumObjectClassConstructors = Array.getLength(Object.class.getConstructors());
	   		final int NumInterfaceConstructors = Array.getLength(NGramContainer.class.getConstructors());
	   		final int NumNGramNodeClassConstructors = Array.getLength(NGramNode.class.getConstructors());
	   		assertTrue("obj:"+NumObjectClassConstructors+":inter:"+NumInterfaceConstructors+" 1 (extra) = class:"+NumNGramNodeClassConstructors,
	   				(NumObjectClassConstructors+NumInterfaceConstructors+ExtraConsCount)==NumNGramNodeClassConstructors);
	   	}
		

	   	@Test
	       public void TOSTRING_ComplexObject() throws NGramException {
	     	   	  DecimalFormat df = new DecimalFormat(NGramContainer.DecFormat);
	    	   	  String test = "be or not to | be : 0.136059\n" + "be or not to | mention : 0.066563\n" + 
	    	   			  		"be or not to | exceed : 0.032759\n" + "be or not to | say : 0.028824\n" +
	    	   			  		"be or not to | the : 0.024524\n";
	    	   	  toTest.setContext("be or not to");
	    	   	  toTest.setPredictions(new String[]{"be","mention","exceed","say","the"});
	    	   	  toTest.setProbabilities(new Double[]{0.13605912332,0.066563234345,0.03275912314,0.028823899932,0.0245242343});
	    	   	  String str = toTest.toString(); 
	    	      assertEquals(test,str);
	       }


}
