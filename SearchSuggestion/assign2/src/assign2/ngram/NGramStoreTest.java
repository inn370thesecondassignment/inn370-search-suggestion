package assign2.ngram;

import static org.junit.Assert.*;

import java.lang.reflect.Array;

import org.junit.Test;
import org.junit.Before;

/**
 * @author Hunaynah Al Naamany N8820848
 * 
 */

/*
 * 1.For the getNGramsFromService method, I think shoud be add some test for it.
 * 	Because there are two conditions, you can check the API and my code to consider what's test should be added
 * 
 * 2.I think both StroeTest and NodeTest files we shold add some test for ToString methods
 * 
 * Mingzhuo Liu
 */
public class NGramStoreTest {

	NGramStore gStore;
	NGramContainer gContainer;
	String context = "the test context";
	String[] words = {"the", "test", "context"};
	String[] predictions = {"to", "the", "try", "why"};
	Double[] probabilities = {0.247159, 0.055452, 0.143869, 0.017713};

	@Before
	public void setUp() throws NGramException {
		gStore = new NGramStore();
		gStore.getNGramsFromService("the test context", 5);
	}
	
	@Test
	public void getNGramsFromService() throws NGramException {
		boolean test = gStore.getNGramsFromService("be or not to", 5);
		assertTrue("testing true", test);		
	}
	
	@Test
	public void getNGram() throws NGramException {
		NGramContainer gContainer = gStore.getNGram(context);
		assertEquals(gContainer.getContext(), context);
	}
	
	@Test
	public void getNullContextNGram() throws NGramException {
		context=null;
		gStore.getNGram(context);
		assertEquals(context, null);
		}
	
	@Test
	public void getEmptyContextNGram() throws NGramException {
		context="";
		gStore.getNGram(context);
		assertEquals(context, "");
		}
	
	@Test
	public void getNGramContext() throws NGramException {
		context="Test";
		gStore.getNGram(context);
		assertEquals(context, "Test");
		}
	
	@Test
	public void addRemoveNGram() throws NGramException {
		NGramNode gNode1 = new NGramNode(context, predictions, probabilities);
		gStore.addNGram(gNode1);
		gStore.removeNGram(context);
		assertEquals(context, context);
	}
	
	/*
   	 * Confirm that the API spec has not been violated through the
   	 * addition of public fields, constructors or methods that were
   	 * not requested
   	 */
   	@Test
   	public void NoExtraPublicMethods() {
   		//Extends Object, implements NGramMap
   		final int toStringCount = 1;
   		final int NumObjectClassMethods = Array.getLength(Object.class.getMethods());
   		final int NumInterfaceMethods = Array.getLength(NGramMap.class.getMethods());
   		final int NumNGramStoreClassMethods = Array.getLength(NGramStore.class.getMethods());
   		assertTrue("obj:"+NumObjectClassMethods+":inter:"+NumInterfaceMethods+" - 1 (toString()) = class:"+NumNGramStoreClassMethods,
   				(NumObjectClassMethods+NumInterfaceMethods-toStringCount)==NumNGramStoreClassMethods);
   	}
   	
   	@Test 
   	public void NoExtraPublicFields() {
   	//Extends Object, implements NGramMap
   		final int NumObjectClassFields = Array.getLength(Object.class.getFields());
   		final int NumInterfaceFields = Array.getLength(NGramMap.class.getFields());
   		final int NumNGramStoreClassFields = Array.getLength(NGramStore.class.getFields());
   		assertTrue("obj + interface = class",(NumObjectClassFields+NumInterfaceFields)==NumNGramStoreClassFields);
   	}
   	
   	@Test 
   	public void NoExtraPublicConstructors() {
   	//Extends Object, implements NGramMap
   		final int NumObjectClassConstructors = Array.getLength(Object.class.getConstructors());
   		final int NumInterfaceConstructors = Array.getLength(NGramMap.class.getConstructors());
   		final int NumNGramStoreClassConstructors = Array.getLength(NGramStore.class.getConstructors());
   		assertTrue("obj:"+NumObjectClassConstructors+":inter:"+NumInterfaceConstructors+" = class:"+NumNGramStoreClassConstructors,
   				(NumObjectClassConstructors+NumInterfaceConstructors)==NumNGramStoreClassConstructors);
   	}
	
}
