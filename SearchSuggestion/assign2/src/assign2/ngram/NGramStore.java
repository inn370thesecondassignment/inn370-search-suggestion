package assign2.ngram;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import assign2.examples.ngram.SimpleNGramGenerator;

import com.microsoft.research.webngram.service.GenerationService;
import com.microsoft.research.webngram.service.NgramServiceFactory;
import com.microsoft.research.webngram.service.GenerationService.TokenSet;

/**
 * @author Mingzhuo LIU N8735751
 * 
 */
public class NGramStore implements NGramMap {
	
	// Key supplied
	private static final String Key = "068cc746-31ff-4e41-ae83-a2d3712d3e68";
   
      //Should use HasMap instaed of Array Please check

	// Create an arrayList ngrams
	//ArrayList<NGramContainer> ngrams = new ArrayList<NGramContainer>();
	//Create a Hashmap ngrams
	HashMap<String, NGramContainer> ngrams = new HashMap<String, NGramContainer>();
	
	/**
	 * The simple parameterless constructor
	 */
	public NGramStore(){}
	
	/**
	 * 
	 * (Silently) Add an ngram to the Map. If the <code>context</code> does not exist in the Map, 
	 * the entry is added.<br>
	 * If the <code>context</code> exists in the Map, then the associated ngram is 
	 * updated (and thus overwritten). 
	 * 
	 * @param ngram - ngram to be added 
	 */
	@Override
	public void addNGram(NGramContainer ngram) {
		
		
		ngrams.put(ngram.getContext(), ngram);
	}

	/**
	 * 
	 * (Silently) Remove an ngram from the Map. If the <code>context</code> does not exist in the Map, 
	 * the entry is not removed, but no status is returned. We guarantee that the entry no longer exists<br>
	 * If the <code>context</code> exists in the Map, then the associated ngram is removed. 
	 * 
	 * @param context - context string for ngram to be removed
	 */
	@Override
	public void removeNGram(String context) {
		
		// TODO Auto-generated method stub
		ngrams.remove(context);
	}

	/**
	 * 
	 * Find the NGram associated with the context if it exists in the Map. 
	 * Return null if the context is not a key in the Map. 
	 * 
	 * @param context
	 * @return NGramContainer associated with the context or null 
	 */
	@Override
	public NGramContainer getNGram(String context) {
		
		// TODO Auto-generated method stub
		
		if (ngrams.get(context) != null) 
		{
			return ngrams.get(context);
		}
		return null;
	}

	/**
	 * 
	 * Get the top maxResults ngrams returned from the service.  
	 * 
	 * @param context - the context for the ngram search 
	 * @param maxResults - the maximum number of 
	 * @return true and store the NGram in the Map if the service returns at least one result<br>
	 * return false and do not store the bare context if the service returns no predictions
	 * @throws NGramException if the service fails to connect or if the NGramContainer cannot be 
	 * created. 
	 */
	@Override
	public boolean getNGramsFromService(String context, int maxResults) throws NGramException {
		
		// Create the service object using defined key
		NgramServiceFactory factory = NgramServiceFactory.newInstance(Key);
		
		//If the key unavailable mention user
		if (factory == null)
		{
			throw new NGramException("The service fails to connect!\n\n");
		}
		
		GenerationService service = factory.newGenerationService();
		
		service.getModels();
		
		//Use 2013-12/5 model
		TokenSet tokenSet = service.generate(SimpleNGramGenerator.Key, "bing-body/2013-12/5", context,	maxResults, null);
		
		//Create a tokenStrs string array to store words which are got from the service
		String[] tokenWords = new String[tokenSet.getWords().size()];
		
		//Store the predictions which are from service to tokenWords array
		for (int i = 0, j = tokenSet.getWords().size(); i < j; i++) 
		{
			tokenWords[i] = tokenSet.getWords().get(i);
		}
		
		List<Double> logProbs = tokenSet.getProbabilities();
		List<Double> probs = new ArrayList<Double>();
		
		for (Double x : logProbs) 
		{
			probs.add(Math.pow(10.0, x));
		}
		
		//Store the data which is from probs list to probsArray array
		Double[] probsArray = new Double[probs.size()];
		
		for (int i = 0; i < probs.size(); i++) 
		{
			probsArray[i] = probs.get(i);
		}
		
		//When service returns not empty result(at least one result) return true and store the ngram
		if (!tokenSet.getWords().isEmpty()) 
		{
			if (getNGram(context) == null) 
			{
				NGramContainer nGramNode = new NGramNode(context,tokenWords,probsArray);
				
				this.addNGram(nGramNode);
				return true;
			}
		}
		//Otherwise return false and mention user the NGramContainer cannot be created
		else
		{
			if (getNGram(context) == null)
			{
				throw new NGramException("NGramContainer cannot be created!!\n\n");
			}
		}
		return false;
	}

	/**
	 * 
	 * ToString method
	 * 
	 * @return -the results
	 */
	@Override
	public String toString() {
		
		String formatedResult = "";
		
		Iterator<NGramContainer> iterator = ngrams.values().iterator();
		while(iterator.hasNext()) 
		{
			formatedResult += iterator.next().toString() + "\n";
		}
		return formatedResult;
}
}