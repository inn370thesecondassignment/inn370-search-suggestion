package assign2.ngram;
import java.text.DecimalFormat;

/**
 * @author Mingzhuo LIU N8735751
 * 
 */
public class NGramNode implements NGramContainer {

	String context;
	String[] predictions;
	Double[] probabilities;

	/**
	 * 
	 * Default constructor
	 */
	//public NGramNode() {}

	/**
	 * 
	 * Constructor 1
	 * 
	 * @param words - array of words in order that make up the context
	 * @param predictions - array of next words in the phrase as predicted by the model
	 * @param probabilities - corresponding probabilities of context>prediction w.r.t. model
	 * @throws NGramException - if words is null or empty or contains at least one empty or
	 *             null string predictions is null or empty or contains at least one empty or 
	 *             null string probabilities is null or contains at least one entry which is
	 *             null, zero, negative or greater than 1.0 or predictions.length is 
	 *             different from probabilities.length
	 */
	public NGramNode(String[] words, String[] predictions,
			Double[] probabilities) throws NGramException {
		if (predictions.length == probabilities.length)
		{
			setContext(words);
			setPredictions(predictions);
			setProbabilities(probabilities);
		}
		else
		{
			throw new NGramException("The predictions array's length is different from probabilities array's length!!\n\n"); 
		}
	}

	/**
	 * 
	 * Constructor 2
	 * 
	 * @param context - string containing the context phrase
	 * @param predictions - array of next words in the phrase as predicted by the model
	 * @param probabilities - corresponding probabilities of context>prediction w.r.t. model
	 * @throws NGramException - if context is null or empty predictions is null or empty or
	 *             contains at least one empty or null string probabilities is null or
	 *             contains at least one entry which is null , zero, negative or 
	 *             greater than 1.0 or predictions.length is different from probabilities.length
	 */
	public NGramNode(String context, String[] predictions,
			Double[] probabilities) throws NGramException {
		
		if (predictions.length == probabilities.length)
		{
			setContext(context);
			setPredictions(predictions);
			setProbabilities(probabilities);
		}
		else
		{
			throw new NGramException("The predictions array's length is different from probabilities array's length!!\n\n"); 
		}
	}

	/**
	 * 
	 * Simple getter method for the context string
	 * 
	 * @return String containing context phrase for predictions
	 */
	@Override
	public String getContext() {
		// TODO Auto-generated method stub
		return context;
	}

	/**
	 * 
	 * Simple setter method for the context string
	 * 
	 * @param context - single String containing context phrase for predictions
	 * @throws NGramException if <code>context</code> is null or empty
	 */
	@Override
	public void setContext(String context) throws NGramException {
		// TODO Auto-generated method stub
		if (context == null)
		{
			throw new NGramException("The context array is null!!\n\n");
		}
		else if (context.isEmpty())
		{
			throw new NGramException("The context array is empty!\n\n");
		}
		else
		{
			this.context = context;
		}
	}

	/**
	 * 
	 * Simple setter method for the context string from multiple words
	 * 
	 * @param words - array of words in order that make up the context
	 * @throws NGramException if <code>words</code> is null or empty or contains at least one empty or null string
	 */
	@Override
	public void setContext(String[] words) throws NGramException {
		// TODO Auto-generated method stub
		
		context = "";
		
		if (words == null)
		{	
			throw new NGramException("The words array is null!\n\n");
		}
		else if (words.length == 0)
		{
			throw new NGramException("The words array is empty!\n\n");
		}
		else
		{
			for (int i = 0; i < words.length; i++) 
			{
				if (words[i] == null)
				{
					throw new NGramException("The words array contains at least one null strings!\n\n");
				}
				else if (words[i].isEmpty())
				{
					throw new NGramException("The words array contains at least one empty strings!\n\n");
				}
				else
				{
					context += words[i] + " ";
				}
			}
		}
		
		context = context.trim();
	}

	/**
	 * 
	 * Simple getter method for the prediction strings
	 * 
	 * @return array of alternative next words in the phrase as predicted by the model
	 */
	@Override
	public String[] getPredictions() {
		// TODO Auto-generated method stub
		return predictions;
	}

	/**
	 * 
	 * Simple setter method for the predictions string array
	 * 
	 * @param predictions - next word in the phrase as predicted by the model
	 * @throws NGramException if <code>predictions</code> is null or empty or contains at least one empty or null string
	 */
	@Override
	public void setPredictions(String[] predictions) throws NGramException {
		// TODO Auto-generated method stub
		
		//To store valid predictions
		String[] validPred;
		
		if (predictions == null)
		{
			throw new NGramException("The predictions array is null!\n\n");
		}
		else if (predictions.length == 0)
		{
			throw new NGramException("The predictions array is empty!\n\n");
		}
		else
		{
			//Initial validPred array
			validPred =  new String[predictions.length];
			
			for (int i = 0; i < predictions.length; i++) 
			{
				if (predictions[i] == null) 
				{
					throw new NGramException("The predictions array contains at least one null strings!\n\n");
				}
				else if (predictions[i].isEmpty())
				{
					throw new NGramException("The predictions array contains at least one empty strings!\n\n");
				}
				else
				{
					//Add valid data to valiPred array
					validPred[i] = predictions[i];
				}
			}
			//Store the prediction
			this.predictions = validPred;
		}
	}

	/**
	 * 
	 * Simple getter method for the probabilities
	 * 
	 * @return array of probabilities of context>prediction w.r.t. model
	 */
	@Override
	public Double[] getProbabilities() {
		// TODO Auto-generated method stub
		return probabilities;
	}

	/**
	 * 
	 * Simple setter method for the probabilities 
	 * 
	 * @param probabilities - array of probabilities of context>prediction w.r.t. model
	 * @throws NGramException if <code>probabilities</code> null or contains at least one  entry which is null , zero, negative or greater than 1.0
	 */
	@Override
	public void setProbabilities(Double[] probabilities) throws NGramException {
		// TODO Auto-generated method stub
		//To store valid probabilities
		Double[] validProb;
		if (probabilities == null)
		{
			throw new NGramException("probabilities is null");
		}
		else
		{
			//Initial validProb array
			validProb = new Double[probabilities.length];
			
			for (int i = 0; i < probabilities.length; i++) 
			{
				if (probabilities[i] == null)
				{
					throw new NGramException("probabilities contains at least one entry which is null");
				}
				else if (probabilities[i] == 0)
				{
					throw new NGramException("probabilities contains an entry which is zero");
				
				}
				else if (probabilities[i] < 0)
				{
					throw new NGramException("probabilities contains an entry which is negative");
				}
				else if (probabilities[i] > 1.0)
				{
					throw new NGramException("probabilities contains an entry which is greater than 1.0");
				}
				else
				{
					//Add valid data to validProb array
					validProb[i] = probabilities[i];
				}
			}
			//Store the valid probabilities
			this.probabilities = validProb;
		}
		
	}

	/**
	 * 
	 * ToString method for the result
	 * 
	 * @return - formatted results
	 */
	@Override
	public String toString() {
		
		String s = "";
		
		DecimalFormat df = new DecimalFormat(DecFormat);
		
		for (int i = 0; i < predictions.length; i++) 
		{
			s += context + " | " + predictions[i] + " : " + df.format(probabilities[i]) + "\n";
		}
		return s;
	}
}
